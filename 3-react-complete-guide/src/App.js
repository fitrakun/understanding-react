import './App.css';
import Expenses from './components/Expenses/Expenses';

function App() {
  const expenses = [
    {
      id: 'e1',
      title: 'Car Insurance 1',
      amount: 300.05,
      date: new Date(2021,3,12)
    },
    {
      id: 'e2',
      title: 'Car Insurance 2',
      amount: 294.67,
      date: new Date(2021,2,12)
    },{
      id: 'e3',
      title: 'Car Insurance 3',
      amount: 414.05,
      date: new Date(2021,4,12)
    },
    {
      id: 'e4',
      title: 'Car Insurance 4',
      amount: 190.34,
      date: new Date(2021,5,12)
    }
  ]
  return (
    <div className="App">
      <h2>Let's get started!</h2>
      <Expenses expenses={expenses}/>
    </div>
  );
}

export default App;
